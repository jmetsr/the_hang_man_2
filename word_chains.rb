class WordChainer
  def initialize
    @dictionary = File.readlines("dictionary.txt").map{|word| word.chomp}
    @current_words = []
    @all_seen_words = {}
  end

  def adjacent_words(word)
    correct_size_words = @dictionary.select{|str| str.length == word.length}
    one_offs = []
    correct_size_words.each do |mila|
      letters_off = 0
      mila.each_char.with_index do |chr,index|
        letters_off += 1 if word[index] != chr
      end

      one_offs << mila if letters_off == 1
    end

    one_offs
  end

  def run(source,target)
    @current_words << source
    @all_seen_words[source] = nil
    while @current_words != []

      new_current_words = loop_through_current_words


      print_path(source,target) if new_current_words.include?(target)
      @current_words = new_current_words
    end
  end

  def loop_through_current_words
    new_current_words = []
    @current_words.each do |current_word|
      adjacent_words(current_word).each do |word|
        if @all_seen_words.include?(word)
          next
        else
          new_current_words << word
          @all_seen_words[word] = current_word
        end
      end
    end

    new_current_words
  end

  def print_path(source,target)
    get_to = target
    while get_to != source
      puts get_to
      get_to = @all_seen_words[get_to]
    end
  end

end