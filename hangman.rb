
class Game

  attr_reader :display
  def initialize(guessing_player,checking_player)
    @guessing_player = guessing_player
    @checking_player = checking_player
  end

  def play
    begin
      size = @checking_player.length
    rescue ArgumentError => e
      puts "Error was : #{ e.message }"
      retry
    end


    @display = "_"*size
    while @display.include?("_")
      puts @display
      guess = @guessing_player.guess(display)
      indices = @checking_player.places(guess)
      (0..size).to_a.each do |index|
        if indices.include?(index)
          @display[index] = guess
        end
      end
    end
    puts @display
  end

end

class HumanPlayer
  def length
    puts "How long is your word?"
    length = gets.chomp.to_i
    raise ArgumentError.new "enter a non zero integer" if length == 0
  end
  def guess(display)
    puts "What letter do you guess?"
    return gets.chomp
  end
  def places(guess)
    puts "Type all indices where this letter apears in your word"
    puts "Put a space between each index"
    gets.chomp.split.map{|num| num.to_i}
  end
end

class ComputerPlayer
  attr_reader :word, :guessed_letters

  def initialize
    @guessed_letters = []
  end

  def length
    words = File.readlines("dictionary.txt").map{|word| word.chomp}
    @word = words.sample
    return @word.length
  end

  def guess(display)
    letter = best_letter(display)
    @guessed_letters << letter
    puts "I guess #{letter}"
    return letter
  end

  def places(guess)
    arr = []
    @word.each_char.with_index do |char,index|
      arr << index if char == guess
    end

    return arr
  end

  def best_letter(display)
    hash = Hash.new(){0}
    words = possible_words(display)
    letters = make_letter_array(words)
    letters.each do |letter|
      hash[letter] += 1
    end
    hash = hash.select{|key,value| !@guessed_letters.include?(key)}
    hash = hash.select{|key,value|letters.include?(key)}

    return max_val(hash)
  end


  def make_letter_array(word_array)
    chars = []
    word_array.each do |word|
      word.each_char do |chr|
        chars << chr
      end
    end

    chars
  end
end

def max_val(hash)
  maxi = hash.values.max
  big_keys = hash.keys.select{|key| hash[key]== maxi}
  big_keys[0]
end

def possible_words(display)
  words = File.readlines("dictionary.txt").map{|word| word.chomp}
  new_words = words.select do |word|
    word.length == display.length
  end

  better_words = []
  words.each_with_index do |word,index|
    if display[index] == word[index] || display[index] == "_"
      better_words << word
    end
  end

  better_words
end

h = HumanPlayer.new
c = ComputerPlayer.new
g = Game.new(c,h)
puts c.guessed_letters
g.play